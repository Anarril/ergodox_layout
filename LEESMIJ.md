
* voor flashen eerst compilen met ./compile.sh
* flash scripts gaan er van uit dat linker hand rechtstreeks naar PC gaat, dus rechter-hand script heeft extra pauze om kabel te switchen
* als string om bepaalde key te activeren niet bekend is:
	* "~/Applications/kiibohd-configurator" opstarten
		* installeer evt. hier vandaan: https://github.com/kiibohd/configurator/releases
	* gewenste key ergens op zetten
	* rechtsboven op "View raw JSON" klikken
	* code opzoeken

